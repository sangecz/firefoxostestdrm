/**
 * Created by sange on 19/07/16.
 */

(function() {

//// Playready+SS
    var TYPE = 'application/vnd.ms-sstr+xml';
// var URL = 'http://w01.quickmedia.tv/vodx/sample.mp4/Manifest'; // bez DRM
// var URL = 'http://w01.quickmedia.tv/vodx/smil:sampledrm.ism/Manifest'; // s DRM
    var URL = 'http://w01.quickmedia.tv/alza-vod/_definst_/smil:alza-vod/alza-vod/e2/98/e2988382-2959-4ae2-a38d-8913524ce853/vrteti_zenou_PIaP_web.smil/Manifest?id=YPlxOF1oNFEzyK-03&amp;audioindex=0'; // s DRM
    var USER_DATA = '5U2VLgkzmrkkINHpR7kb+w4XYYDCbUKgK3bDXcFxN9ewSLoEAPEUKvUNiAjBOqrWpWoapJ9DUjeVx28aFzhowpoNjYJKHek1s2bbh5OEStAUdIcufwauv4wDcLlRvm/bTUngJk5S1afzH43wNip+kAf6d5bwaHPzhEAb8ofCY9E=';
    var CUSTOM_DATA = '15402|20357|' + USER_DATA;
// var LA_URL = 'http://playready.directtaps.net/pr/svc/rightsmanager.asmx?PlayRight=1&UseSimpleNonPersistentLicense=1&ContentKey=yTSWCAyY4XIkzRDct0nwiQ==';
// var CUSTOM_DATA = '';
    var LA_URL = 'http://license.quickmedia.tv/vuRights/RightsManager.asmx?project=v2.vuEasy';
    var CUSTOM_HEADER = '<SetHttpHeader><HttpHeader>X-CustomData: ' + CUSTOM_DATA + '</HttpHeader></SetHttpHeader>';

    // your page initialization code here
    // the DOM will be available here
    console.log('start....' + 30);

    // send licence request using sendDRMMessage and the right MIME type

    function sendLicenceRequest(){

        // The PlayReady Initiator is created by the content server.
        var DRMSystemID = "urn:dvb:casystemid:19219";
        var xmlLicenceAcquisition = '<?xml version="1.0" encoding="utf-8"?>'
            + '<PlayReadyInitiator xmlns= "http://schemas.microsoft.com/DRM/2007/03/protocols/">'
//            + CUSTOM_HEADER
            + '<LicenseAcquisition>'
            + '<Header>'
            + '<WRMHEADER xmlns= "http://schemas.microsoft.com/DRM/2007/03/PlayReadyHeader" version="4.0.0.0">'
            + '<DATA>'
            + '<PROTECTINFO>'
            + '<KEYLEN>16</KEYLEN>'
            + '<ALGID>AESCTR</ALGID>'
            + '</PROTECTINFO>'
            + '<LA_URL>' + LA_URL + '</LA_URL>'  // vklada SDK
            // + '<KID></KID>'                      // vklada SDK
            // + '<CHECKSUM></CHECKSUM>'            // vklada SDK
            + '</DATA>'
            + '</WRMHEADER>'
            + '</Header>'
            + '</LicenseAcquisition>'
	    + '<SetCustomData>'
            + '<CustomData>' + CUSTOM_DATA + '</CustomData>'
	    + '</SetCustomData>'
            + '</PlayReadyInitiator>';

        var pluginElement = document.getElementById('drmplugin');
        pluginElement.onDRMMessageResult = HandleOnDRMMessageResult;
        pluginElement.onDRMRightsError = HandleOnDRMRightsError;
        pluginElement.sendDRMMessage('application/vnd.ms-playready.initiator+xml',
            xmlLicenceAcquisition, DRMSystemID);
    }

    function HandleOnDRMMessageResult(msgID, resultMsg, resultCode) {
        var error = "";
        console.log("HandleOnDRMMessageResult msgID:" + msgID + " resultMsg:" + resultMsg + " resultCode: " + resultCode);

        if (resultCode == 0) {

        } else {
            try {
                var videoid = document.getElementById("videoid");
                videoid.play();
            } catch (e) {
                console.log("HandleOnDRMMessageResult() :" + e.message);
            }
            switch (resultCode) {
                //used for printing the correct error message
                case 1:
                    error = "Unknown error";
                    error += " - " + HtmlEncode(resultMsg);
                    break;
                case 2:
                    error = "Cannot process request";
                    break;
                case 3:
                    error = "Unknown MIME type";
                    break;
                case 4:
                    error = "User Consent Needed";
                    break;

            }
            console.log("Playback failed. Error:" + error);
        }
    }

    function HandleOnDRMRightsError(msgID, resultMsg, resultCode) {
        console.log("HandleOnDRMRightsError msgID:" + msgID + " resultMsg:" + resultMsg + " resultCode: " + resultCode);
    }

    sendLicenceRequest();

    var sourceElem = document.createElement("source");
    sourceElem.src = URL;
    sourceElem.type = TYPE;

    var videoElem = document.getElementById("videoid");
    videoElem.appendChild(sourceElem);

})();


