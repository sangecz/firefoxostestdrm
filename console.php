
	<?php
		// console.php
		
		//--------------------------------------------------------------------------------
		function unescape($str, $encoding = 'BIG-5') {	
			if (!$str) { return ''; }

			$str = rawurldecode($str);
			
			// The result stores in a $r object
			preg_match_all('/%u.{4}|.{4};|&#\d+;|.+/U', $str, $r);
			
			// Get the first element only
			$ar = $r[0];

			foreach($ar as $k => $v) {	
				if (substr($v, 0, 2) == '%u') {
					$ar[$k] = iconv('UCS-2', $encoding, pack('H4', substr($v, -4)));
				}
				else if(substr($v, 0, 3) == '') {	
					$ar[$k] = iconv('UCS-2' , $encoding, pack('H4', substr($v, 3, -1)));
				}
				else if(substr($v, 0, 2) == '&#') {	
					$ar[$k] = iconv('UCS-2', $encoding, pack('n', substr($v, 2, -1)));
				}
			}
			return join('', $ar);
		}
		//--------------------------------------------------------------------------------
		
		
		
				
		if (isset($_POST['msg'])) { $msg = $_POST['msg']; }
		if (isset($_POST['logFilePath'])) { $logFilePath = $_POST['logFilePath']; }
		else { $logFilePath = ""; }
		
		if (!isset($msg)) { exit; }
	
		$filePath = strlen($logFilePath) > 0 ? urldecode($logFilePath) : "console.log";

		// Open to read data
		if (file_exists($filePath)) {
			$current = file_get_contents($filePath);
		}
		else {
			$current = '';
		}
		
		if (!$current) { $current = ''; }
		
//		$msg = urldecode($msg);
		$msg = unescape($msg);
		
		// Append data
		$current .= $msg . "\n";
		
		// Write msg to file
		file_put_contents($filePath, $current);
	?>