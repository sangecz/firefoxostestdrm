
	/*
		Created by Accedo on 2012/10/18
	*/

	// console.js
	(function(win) {
		var console = {},
		
			// Point out the location of console.php file
			PHP_FILE_PATH = "console.php",
			
			// Example
//			PHP_FILE_PATH = "../console.php",
			
			
			// Point out the location of console.log file
			LOG_FILE_PATH = "./../debug/console.log";
			
			// Example
//			LOG_FILE_PATH = "../../../cgi-bin/tmp/console.log";
			
			
		
		
		//--------------------------------------------------------------------------------
		// Internal use only
		console._log = function(msg) {
			var now = new Date().toISOString();
			msg = '[' + now + '] ' + msg;
			typeof msg === "null" && (msg = "null");
			typeof msg === "undefined" && (msg = "undefined");
			typeof msg === "object" && (msg = "[object]");
			
			if (typeof msg === "boolean") {
				msg = msg ? "true" : "false";
			}

//			msg = "msg=" + encodeURIComponent(msg);
			msg = "msg=" + escape(msg) + "&logFilePath=" + escape(LOG_FILE_PATH);
			
			var ajax;
			
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				ajax = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				ajax = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			ajax.onreadystatechange = function() {
				if (ajax.readyState === 4 && ajax.status === 200) {
					var retDat = ajax.responseText,
						spaces = "	, ,\r\n,\r,\n".split(","),
						size = spaces.length,
						ch;
				
					if (retDat) {
						while (size--) {
							ch = spaces[size];
							
							while (retDat.indexOf(ch) != -1) {
								retDat = retDat.replace(ch, "");
							}
						}
						
						if (retDat) {
							alert(retDat);					
						}
					}
					
					ajax = null;
				}
			}

			ajax.open("POST", PHP_FILE_PATH, true);
			ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//			ajax.setRequestHeader("Content-length", msg.length);
//			ajax.setRequestHeader("Connection", "close");			
			ajax.send(msg);			

		};
		//--------------------------------------------------------------------------------
		
		
		
		//--------------------------------------------------------------------------------
		// Expose to outside
		console.log = function(msg) {
			window.setTimeout(function() {
				window.console._log(msg);
				msg = null;
			}, 500);
		};
		//--------------------------------------------------------------------------------
		
		win.Console = win.console;
		win.console = console;
	})(window);
	
	
	
	
	
	
	
	